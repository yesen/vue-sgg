export default {
  install(Vue) {
    Vue.filter("top4", (v) => v.slice(0, 4));
    Vue.directive("focus-bind", {
      bind(e, { value }) {
        e.value = value;
      },
      inserted(e) {
        e.focus();
      },
      update(e, { value }) {
        e.value = value;
      },
    });
    Vue.mixin({
      mounted() {
        console.log("mounted");
      },
    });
    console.log(Vue);

    Vue.prototype.hello = () => {
      alert("你好呀");
    };
  },
};
