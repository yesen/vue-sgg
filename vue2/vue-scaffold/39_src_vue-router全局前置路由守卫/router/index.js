import VueRouter from "vue-router";

import About from "../pages/About.vue";
import Home from "../pages/Home/index.vue";
import HomeNews from "../pages/Home/News.vue";
import HomeMessage from "../pages/Home/Message/index.vue";
import HomeMessageDetail from "../pages/Home/Message/Detail.vue";
import Login from "../pages/Login.vue";

const router = new VueRouter({
  routes: [
    {
      name: "about",
      path: "/about",
      component: About,
    },
    {
      name: "home",
      path: "/home",
      component: Home,
      children: [
        {
          name: "homeNews",
          path: "news",
          component: HomeNews,
          meta: { needAuth: true },
        },
        {
          name: "homeMessage",
          path: "message",
          component: HomeMessage,
          meta: { needAuth: true },
          children: [
            {
              name: "homeMessageDetail",
              path: "detail",
              component: HomeMessageDetail,
              meta: { needAuth: true },

              // props 第一种写法：值为对象。--> 只能传递静态数据
              // props: {foo:'abc', bar:'xyz'},

              // props 第二种写法：值为布尔值。如果是 true，则把所有 params 参数作为 props 自动传递给组件
              // props: true,

              // props 第三种写法：值为函数
              props($route) {
                return {
                  id: $route.query.id,
                  title: $route.query.title,
                };
              },
            },
          ],
        },
      ],
    },
    {
      name: "login",
      path: "/login",
      component: Login,
    },
  ],
});

// 全局、前置路由守卫——初始化时、每次路由切换之前调用
router.beforeEach((to, from, next) => {
  // console.log("前置路由守卫", to, from, next);
  // console.log(to.name, to.path);

  // if (/^home\w+?/.test(to.name)) {
  if (to.meta.needAuth) {
    if (window.localStorage.getItem("auth") !== "yesen") {
      return next({
        name: "login",
        query: { from: to.name, args: JSON.stringify(to.query) },
      });
    }
  }
  next();
});
router.afterEach(() => {});

export default router;
