import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// 用于响应组件中动作
const actions = {
  increment(context, value) {
    context.commit("Increment", value);
  },
  decrement({ commit }, value) {
    commit("Decrement", value);
  },
  incrementOdd({ commit, state }, value) {
    if (state.sum % 2) {
      commit("Increment", value);
    }
  },
  incrementWait({ commit }, value) {
    const t = setTimeout(() => {
      commit("Increment", value);
    }, 500);
  },
};

// 用于操作数据（state）
const mutations = {
  Increment(state, value) {
    state.sum += value;
  },
  Decrement(state, value) {
    state.sum -= value;
  },
};

// 用于存储数据
const state = {
  sum: 0,
};

// 用于将 state 中的数据进行加工
const getters = {
  bigSum(state) {
    return state.sum * 10;
  },
};

export default new Vuex.Store({
  actions,
  mutations,
  state,
  getters,
});
