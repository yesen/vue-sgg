import axios from "axios";

export default {
  namespaced: true,
  actions: {
    addFirstNameMustBeWang({ commit }, value) {
      if (value.name.indexOf("王") === 0) {
        commit("AddPerson", value);
      }
    },
    getClientIp({ commit }) {
      axios
        .get("https://httpbin.org/ip")
        .then(({ data }) => {
          commit("GetClientIp", data.origin);
        })
        .catch((e) => {
          commit("GetClientIp", e.toString());
        });
    },
  },
  mutations: {
    AddPerson(state, value) {
      state.personList.unshift(value);
    },
    GetClientIp(state, value) {
      state.ip = value;
    },
  },
  state: {
    personList: [],
    ip: "正在获取",
  },
  getters: {
    firstPersonName(state) {
      return state.personList[0]?.name || "(无)";
    },
  },
};
