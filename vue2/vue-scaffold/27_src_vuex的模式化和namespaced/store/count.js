export default {
  namespaced: true,
  actions: {
    increment(context, value) {
      context.commit("Increment", value);
    },
    decrement({ commit }, value) {
      commit("Decrement", value);
    },
    incrementOdd({ commit, state }, value) {
      if (state.sum % 2) {
        commit("Increment", value);
      }
    },
    incrementWait({ commit }, value) {
      const t = setTimeout(() => {
        commit("Increment", value);
      }, 500);
    },
  },
  mutations: {
    Increment(state, value) {
      state.sum += value;
    },
    Decrement(state, value) {
      state.sum -= value;
    },
  },
  state: {
    sum: 0,
    school: "非正常人类研究所",
    subject: "发癲",
  },
  getters: {
    bigSum(state) {
      return state.sum * 10;
    },
  },
};
