import VueRouter from "vue-router";

import About from "../pages/About.vue";
import Home from "../pages/Home/index.vue";
import HomeNews from "../pages/Home/News.vue";
import HomeMessage from "../pages/Home/Message/index.vue";
import HomeMessageDetail from "../pages/Home/Message/Detail.vue";

export default new VueRouter({
  routes: [
    {
      name: "about",
      path: "/about",
      component: About,
    },
    {
      path: "/home",
      component: Home,
      children: [
        {
          path: "news",
          component: HomeNews,
        },
        {
          path: "message",
          component: HomeMessage,
          children: [
            {
              name: "homeMessageDetail",
              path: "detail/:id/:title",
              component: HomeMessageDetail,

              // props 第一种写法：值为对象。--> 只能传递静态数据
              // props: {foo:'abc', bar:'xyz'},

              // props 第二种写法：值为布尔值。如果是 true，则把所有 params 参数作为 props 自动传递给组件
              props: true,
            },
          ],
        },
      ],
    },
  ],
});
