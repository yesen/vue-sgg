import Vue from "vue";
import App from "./App.vue";
import { mixin } from "./mixin";

// 全局mixin
Vue.mixin(mixin);

new Vue({
  render: (h) => h(App),
}).$mount("#app");
