import VueRouter from "vue-router";

import About from "../pages/About.vue";
import Home from "../pages/Home/index.vue";
import HomeNews from "../pages/Home/News.vue";
import HomeMessage from "../pages/Home/Message.vue";

export default new VueRouter({
  routes: [
    {
      path: "/about",
      component: About,
    },
    {
      path: "/home",
      component: Home,
      children: [
        {
          path: "news",
          component: HomeNews,
        },
        {
          path: "message",
          component: HomeMessage,
        },
      ],
    },
  ],
});
