import VueRouter from "vue-router";

import About from "../pages/About.vue";
import Home from "../pages/Home/index.vue";
import HomeNews from "../pages/Home/News.vue";
import HomeMessage from "../pages/Home/Message/index.vue";
import HomeMessageDetail from "../pages/Home/Message/Detail.vue";
import Login from "../pages/Login.vue";

const router = new VueRouter({
  routes: [
    {
      name: "about",
      path: "/about",
      component: About,
      meta: { title: "关于" },
    },
    {
      name: "home",
      path: "/home",
      component: Home,
      meta: { title: "主页" },
      children: [
        {
          name: "homeNews",
          path: "news",
          component: HomeNews,
          meta: { needAuth: true, title: "新闻" },
        },
        {
          name: "homeMessage",
          path: "message",
          component: HomeMessage,
          meta: { needAuth: true, title: "消息" },
          children: [
            {
              name: "homeMessageDetail",
              path: "detail",
              component: HomeMessageDetail,
              meta: { needAuth: true, title: "消息详情" },
              // 独享路由守卫
              beforeEnter(to, from, next) {
                // console.log(
                //   `独享路由守卫。to.name: ${to.name}, from.name: ${from.name}`
                // );
                if (from.name !== "homeMessage") {
                  return alert("非法来源");
                }
                next();
              },

              // props 第一种写法：值为对象。--> 只能传递静态数据
              // props: {foo:'abc', bar:'xyz'},

              // props 第二种写法：值为布尔值。如果是 true，则把所有 params 参数作为 props 自动传递给组件
              // props: true,

              // props 第三种写法：值为函数
              props($route) {
                return {
                  id: $route.query.id,
                  title: $route.query.title,
                };
              },
            },
          ],
        },
      ],
    },
    {
      name: "login",
      path: "/login",
      component: Login,
    },
  ],
});
/* 
// 全局、前置路由守卫——初始化时、每次路由切换之前调用
router.beforeEach((to, from, next) => {
  // console.log("前置路由守卫", to, from, next);
  // console.log(to.name, to.path);

  // if (/^home\w+?/.test(to.name)) {
  if (to.meta.needAuth) {
    if (window.localStorage.getItem("auth") !== "yesen") {
      return next({
        name: "login",
        query: { from: to.name, args: JSON.stringify(to.query) },
      });
    }
  }
  next();
});

// 全局、后置路由守卫——初始化时、每次路由切换之后调用
router.afterEach((to, from) => {
  document.title = to.meta.title || "vue app";
});
 */
export default router;
